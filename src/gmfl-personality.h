/* gmfl-personality.h
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GMFL_PERSONALITY_H
#define GMFL_PERSONALITY_H

#include <glib-object.h>

G_BEGIN_DECLS

#define GMFL_TYPE_PERSONALITY (gmfl_personality_get_type())

G_DECLARE_FINAL_TYPE (GmflPersonality, gmfl_personality, GMFL, PERSONALITY, GObject)

GmflPersonality*     gmfl_personality_new                        (gdouble            extroversion,
                                                                  gdouble            stability,
                                                                  gdouble            agreeableness,
                                                                  gdouble            consciousness,
                                                                  gdouble            culturality);

void                 gmfl_personality_get_traits                 (GmflPersonality   *self,
                                                                  gdouble           *extroversion,
                                                                  gdouble           *stability,
                                                                  gdouble           *agreeableness,
                                                                  gdouble           *consciousness,
                                                                  gdouble           *culturality);

G_END_DECLS

#endif /* GMFL_PERSONALITY_H */

