/* gmfl-test-runner.h
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GMFL_TEST_RUNNER_H
#define GMFL_TEST_RUNNER_H

#include "gmfl-types.h"

#include <gio/gio.h>

G_BEGIN_DECLS

gboolean             gmfl_test_runner_run_test_for_file          (GmflApplication    *app,
                                                                  GFile              *file,
                                                                  GCancellable       *cancellable,
                                                                  GError            **error);

G_END_DECLS

#endif /* GMFL_TEST_RUNNER_H */
