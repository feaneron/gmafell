/* gmfl-types.h
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GMFL_TYPES_H
#define GMFL_TYPES_H

#include <glib.h>

G_BEGIN_DECLS

typedef enum
{
  GMFL_PERSONALITY_MODEL_BASELINE,
  GMFL_PERSONALITY_MODEL_B5
} GmflModelType;

typedef struct _GmflApplication              GmflApplication;

typedef struct _GmflModel                    GmflModel;
typedef struct _GmflModelMiner               GmflModelMiner;

typedef struct _GmflPersonality              GmflPersonality;
typedef struct _GmflPersonalityModifier      GmflPersonalityModifier;

typedef struct _GmflWindow                   GmflWindow;

G_END_DECLS

#endif /* GMFL_TYPES_H */
