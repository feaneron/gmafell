/* gmfl-window.c
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "GmflWindow"

#include "gmfl-application.h"
#include "gmfl-model-miner.h"
#include "gmfl-personality.h"
#include "gmfl-personality-modifier.h"
#include "gmfl-window.h"

#include <gwords.h>

struct _GmflWindow
{
  GtkApplicationWindow parent;

  GtkTextView        *text_view;
  GtkTextBuffer      *text_buffer;

  GtkWidget          *baseline_radio;
  GtkWidget          *b5_radio;
  GtkWidget          *empty_mode_box;
  GtkWidget          *rewrite_button;

  GCancellable       *cancellable;
  GwLanguage         *language;

  GmflModelType       model_type;

  /* Personality GtkAdjustments */
  GtkAdjustment      *extroversion_adjustment;
  GtkAdjustment      *stability_adjustment;
  GtkAdjustment      *agreeableness_adjustment;
  GtkAdjustment      *consciousness_adjustment;
  GtkAdjustment      *culturality_adjustment;
};

G_DEFINE_TYPE (GmflWindow, gmfl_window, GTK_TYPE_APPLICATION_WINDOW)

enum
{
  PROP_0,
  PROP_LANGUAGE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS] = { NULL, };

/*
 * Auxiliary methods
 */

static void
load_css_provider (GmflWindow *self)
{
  g_autoptr (GtkCssProvider) css_provider = NULL;
  g_autoptr (GFile) css_file = NULL;
  GtkSettings *settings;
  GtkWidget *widget;
  g_autofree gchar *theme = NULL;
  g_autofree gchar *uri = NULL;

  widget = GTK_WIDGET (self);

  if (!gtk_widget_has_screen (widget))
    {
      g_critical ("The main window has no screen!");
      return;
    }

  /* Retrieve the theme name */
  settings = gtk_settings_get_for_screen (gtk_widget_get_screen (widget));
  g_object_get (settings, "gtk-theme-name", &theme, NULL);

  /* Load in the CSS provider */
  css_provider = gtk_css_provider_new ();

  uri = g_strdup_printf ("resource:///com/feaneron/gmafell/theme/%s.css", theme);
  css_file = g_file_new_for_uri (uri);

  if (g_file_query_exists (css_file, NULL))
    {
      g_debug ("Loading %s.css", theme);
      gtk_css_provider_load_from_file (css_provider, css_file, NULL);
    }
  else
    {

      g_debug ("Loading Adwaita.css");
      gtk_css_provider_load_from_resource (css_provider, "/com/feaneron/gmafell/theme/Adwaita.css");
    }

  /* Apply the CSS */
  gtk_style_context_add_provider_for_screen (gtk_widget_get_screen (widget),
                                             GTK_STYLE_PROVIDER (css_provider),
                                             GTK_STYLE_PROVIDER_PRIORITY_APPLICATION + 1);
}

static GmflPersonality*
get_personality (GmflWindow *self)
{
  GmflPersonality *personality;

  personality = gmfl_personality_new (gtk_adjustment_get_value (self->extroversion_adjustment),
                                      gtk_adjustment_get_value (self->stability_adjustment),
                                      gtk_adjustment_get_value (self->agreeableness_adjustment),
                                      gtk_adjustment_get_value (self->consciousness_adjustment),
                                      gtk_adjustment_get_value (self->culturality_adjustment));

  return personality;
}

/*
 * Callbacks
 */

static void
document_modified_cb (GObject      *source,
                      GAsyncResult *result,
                      gpointer      user_data)
{
  g_autoptr (GwDocument) document;
  GmflWindow *self;
  GwString *new_text;
  GError *error;

  error = NULL;
  self = GMFL_WINDOW (user_data);
  document = GW_DOCUMENT (source);

  if (!gw_document_modify_finish (result, &error))
    {
      g_warning ("Error modifying document: %s", error->message);
      g_clear_error (&error);
      return;
    }

  new_text = gw_document_get_text (document);

  gtk_text_buffer_set_text (self->text_buffer, new_text, -1);
}

static void
radio_button_changed_cb (GtkToggleButton *radio,
                         GParamSpec      *pspec,
                         GmflWindow      *self)
{
  if (!gtk_toggle_button_get_active (radio))
    return;

  if (GTK_WIDGET (radio) == self->b5_radio)
    self->model_type = GMFL_PERSONALITY_MODEL_B5;
  else
    self->model_type = GMFL_PERSONALITY_MODEL_BASELINE;
}

static void
rewrite_button_clicked_cb (GtkButton  *button,
                           GmflWindow *self)
{
  g_autoptr (GmflPersonality) personality = NULL;
  GmflPersonalityModifier *modifier;
  GmflApplication *app;
  GmflModelMiner *miner;
  GtkTextIter start, end;
  GwDocument *document;
  GwString *text;
  GError *error;

  gtk_text_buffer_get_start_iter (self->text_buffer, &start);
  gtk_text_buffer_get_end_iter (self->text_buffer, &end);

  error = NULL;
  text = gw_string_new (gtk_text_buffer_get_text (self->text_buffer, &start, &end, FALSE));
  document = gw_language_create_document_sync (self->language, text, NULL, &error);

  if (error)
    {
      g_warning ("Error creating document: %s", error->message);
      g_clear_error (&error);
      return;
    }

  /* Personality */
  personality = get_personality (self);

  /* Setup the personality modifier */
  app = GMFL_APPLICATION (g_application_get_default ());
  miner = gmfl_application_get_miner (app);

  modifier = gmfl_personality_modifier_new (miner);
  gmfl_personality_modifier_set_personality (modifier, personality);
  gmfl_personality_modifier_set_model_type (modifier, self->model_type);
  gmfl_personality_modifier_set_document (modifier, document);

  gw_document_modify (document,
                      GW_MODIFIER (modifier),
                      self->cancellable,
                      document_modified_cb,
                      self);

}

static void
text_buffer_changed_cb (GtkTextBuffer *buffer,
                        GmflWindow    *self)
{
  gboolean empty = gtk_text_buffer_get_char_count (buffer) == 0;

  gtk_widget_set_visible (self->empty_mode_box, empty);
  gtk_widget_set_sensitive (self->rewrite_button, !empty);
}

/*
 * GObject overrides
 */

static void
gmfl_window_finalize (GObject *object)
{
  GmflWindow *self = (GmflWindow *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);
  g_clear_object (&self->language);

  G_OBJECT_CLASS (gmfl_window_parent_class)->finalize (object);
}

static void
gmfl_window_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  GmflWindow *self = (GmflWindow *) object;

  switch (prop_id)
    {
    case PROP_LANGUAGE:
      g_value_set_object (value, self->language);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_window_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  GmflWindow *self = (GmflWindow *) object;

  switch (prop_id)
    {
    case PROP_LANGUAGE:
      if (g_set_object (&self->language, g_value_get_object (value)))
        g_object_notify_by_pspec (object, properties[PROP_LANGUAGE]);
      g_debug ("Setting language to %s", gw_language_get_language_code (self->language));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_window_class_init (GmflWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gmfl_window_finalize;
  object_class->get_property = gmfl_window_get_property;
  object_class->set_property = gmfl_window_set_property;

  properties[PROP_LANGUAGE] = g_param_spec_object ("language",
                                                   "Current language",
                                                   "The current language being used",
                                                   GW_TYPE_LANGUAGE,
                                                   G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/feaneron/gmafell/ui/window.ui");

  gtk_widget_class_bind_template_child (widget_class, GmflWindow, agreeableness_adjustment);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, baseline_radio);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, b5_radio);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, consciousness_adjustment);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, culturality_adjustment);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, empty_mode_box);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, extroversion_adjustment);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, rewrite_button);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, stability_adjustment);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, text_view);
  gtk_widget_class_bind_template_child (widget_class, GmflWindow, text_buffer);

  gtk_widget_class_bind_template_callback (widget_class, radio_button_changed_cb);
  gtk_widget_class_bind_template_callback (widget_class, rewrite_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, text_buffer_changed_cb);
}

static void
gmfl_window_init (GmflWindow *self)
{
  self->model_type = GMFL_PERSONALITY_MODEL_B5;
  self->cancellable = g_cancellable_new ();

  gtk_widget_init_template (GTK_WIDGET (self));

  load_css_provider (self);
}

GmflWindow *
gmfl_window_new (GmflApplication *application)
{
  return g_object_new (GMFL_TYPE_WINDOW,
                       "application", application,
                       NULL);
}

