/* gmfl-personality.c
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gmfl-personality.h"

struct _GmflPersonality
{
  GObject             parent;

  gdouble             extroversion;
  gdouble             stability;
  gdouble             agreeableness;
  gdouble             consciousness;
  gdouble             culturality;
};

G_DEFINE_TYPE (GmflPersonality, gmfl_personality, G_TYPE_OBJECT)

enum
{
  PROP_0,
  PROP_AGREEABLENESS,
  PROP_CONSCIOUSNESS,
  PROP_CULTURALITY,
  PROP_EXTROVERSION,
  PROP_STABILITY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS] = { NULL, };

static void
gmfl_personality_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  GmflPersonality *self = GMFL_PERSONALITY (object);

  switch (prop_id)
    {
    case PROP_AGREEABLENESS:
      g_value_set_double (value, self->agreeableness);
      break;

    case PROP_CONSCIOUSNESS:
      g_value_set_double (value, self->consciousness);
      break;

    case PROP_CULTURALITY:
      g_value_set_double (value, self->culturality);
      break;

    case PROP_EXTROVERSION:
      g_value_set_double (value, self->culturality);
      break;

    case PROP_STABILITY:
      g_value_set_double (value, self->culturality);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_personality_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  GmflPersonality *self = GMFL_PERSONALITY (object);
  gdouble trait;

  trait = g_value_get_double (value);

  g_return_if_fail (trait >= 1.0 && trait <= 5.0);

  switch (prop_id)
    {
    case PROP_AGREEABLENESS:
      self->agreeableness = trait;
      break;

    case PROP_CONSCIOUSNESS:
      self->consciousness = trait;
      break;

    case PROP_CULTURALITY:
      self->culturality = trait;
      break;

    case PROP_EXTROVERSION:
      self->extroversion = trait;
      break;

    case PROP_STABILITY:
      self->stability = trait;
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_personality_class_init (GmflPersonalityClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = gmfl_personality_get_property;
  object_class->set_property = gmfl_personality_set_property;

  properties[PROP_AGREEABLENESS] = g_param_spec_double ("agreeableness",
                                                        "Agreeableness",
                                                        "Agreeableness",
                                                        1.0,
                                                        5.0,
                                                        3.0,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_CONSCIOUSNESS] = g_param_spec_double ("consciousness",
                                                        "Consciousness",
                                                        "Consciousness",
                                                        1.0,
                                                        5.0,
                                                        3.0,
                                                        G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_CULTURALITY] = g_param_spec_double ("culturality",
                                                      "Culturality",
                                                      "Culturality",
                                                      1.0,
                                                      5.0,
                                                      3.0,
                                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_EXTROVERSION] = g_param_spec_double ("extroversion",
                                                       "Extroversion",
                                                       "Extroversion",
                                                       1.0,
                                                       5.0,
                                                       3.0,
                                                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  properties[PROP_STABILITY] = g_param_spec_double ("stability",
                                                    "Stability",
                                                    "Stability",
                                                    1.0,
                                                    5.0,
                                                    3.0,
                                                    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gmfl_personality_init (GmflPersonality *self)
{
  self->agreeableness = 3.0;
  self->consciousness = 3.0;
  self->culturality = 3.0;
  self->extroversion = 3.0;
  self->stability = 3.0;
}


GmflPersonality*
gmfl_personality_new (gdouble extroversion,
                      gdouble stability,
                      gdouble agreeableness,
                      gdouble consciousness,
                      gdouble culturality)
{
  return g_object_new (GMFL_TYPE_PERSONALITY,
                       "agreeableness", agreeableness,
                       "consciousness", consciousness,
                       "culturality", culturality,
                       "extroversion", extroversion,
                       "stability", stability,
                       NULL);
}

void
gmfl_personality_get_traits (GmflPersonality *self,
                             gdouble         *extroversion,
                             gdouble         *stability,
                             gdouble         *agreeableness,
                             gdouble         *consciousness,
                             gdouble         *culturality)
{
  g_return_if_fail (GMFL_IS_PERSONALITY (self));

  if (extroversion)
    *extroversion = self->extroversion;

  if (stability)
    *stability = self->stability;

  if (agreeableness)
    *agreeableness = self->agreeableness;

  if (consciousness)
    *consciousness = self->consciousness;

  if (culturality)
    *culturality = self->culturality;
}
