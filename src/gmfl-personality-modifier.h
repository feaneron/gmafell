/* gmfl-personality-modifier.h
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GMFL_PERSONALITY_MODIFIER_H
#define GMFL_PERSONALITY_MODIFIER_H

#include "gmfl-types.h"

#include <glib-object.h>

G_BEGIN_DECLS

#define GMFL_TYPE_PERSONALITY_MODIFIER (gmfl_personality_modifier_get_type())

G_DECLARE_FINAL_TYPE (GmflPersonalityModifier, gmfl_personality_modifier, GMFL, PERSONALITY_MODIFIER, GObject)

GmflPersonalityModifier* gmfl_personality_modifier_new             (GmflModelMiner          *miner);

GmflModelType            gmfl_personality_modifier_get_model_type  (GmflPersonalityModifier *self);

void                     gmfl_personality_modifier_set_model_type  (GmflPersonalityModifier *self,
                                                                    GmflModelType            model_type);

GmflPersonality*         gmfl_personality_modifier_get_personality (GmflPersonalityModifier *self);

void                     gmfl_personality_modifier_set_personality (GmflPersonalityModifier *self,
                                                                    GmflPersonality         *personality);

GmflModelMiner*          gmfl_personality_modifier_get_miner       (GmflPersonalityModifier *self);

void                     gmfl_personality_modifier_set_document    (GmflPersonalityModifier *self,
                                                                    GwDocument              *document);

G_END_DECLS

#endif /* GMFL_PERSONALITY_MODIFIER_H */

