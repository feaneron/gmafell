/* gmfl-test-runner.c
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "GmflTestRunner"

#include "gmfl-application.h"
#include "gmfl-test-runner.h"
#include "gmfl-personality.h"
#include "gmfl-personality-modifier.h"

#include <gwords.h>
#include <string.h>

typedef struct _TestData
{
  GmflPersonality    *personality;
  gchar              *text;
} TestData;


/*
 * TestData functions
 */

static void
test_data_free (TestData *data)
{
  g_clear_pointer (&data->text, g_free);
  g_clear_object (&data->personality);

  g_slice_free (TestData, data);
}

static TestData*
test_data_new (gdouble      extroversion,
               gdouble      stability,
               gdouble      agreeableness,
               gdouble      consciousness,
               gdouble      culturality,
               const gchar *text)
{
  TestData *data;

  data = g_slice_new (TestData);
  data->personality = gmfl_personality_new (extroversion, stability, agreeableness, consciousness, culturality);
  data->text = g_strdup (text);

  return data;
}


/*
 * Test functions
 */

static gboolean
load_test_data (GFile         *file,
                GHashTable   **out_test_data,
                GCancellable  *cancellable,
                GError       **error)
{
  g_autoptr (GFileInputStream) input_stream = NULL;
  g_autoptr (GHashTable) test_data = NULL;
  g_autoptr (GFileInfo) file_info = NULL;
  g_autofree gchar *contents = NULL;
  g_autofree GStrv lines = NULL;
  guint64 file_size;
  guint i;

  input_stream = g_file_read (file, cancellable, error);

  if (!input_stream)
    return FALSE;

  /* Retrieve file size */
  file_info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_SIZE, G_FILE_QUERY_INFO_NONE, NULL, error);

  if (!file_info)
    return FALSE;

  file_size = g_file_info_get_attribute_uint64 (file_info, G_FILE_ATTRIBUTE_STANDARD_SIZE);

  /* Read file contents into memory */
  contents = g_malloc0 (file_size);

  if (!g_input_stream_read (G_INPUT_STREAM (input_stream), contents, file_size, NULL, error))
    return FALSE;

  /* Parse each line */
  lines = g_strsplit (contents, "\n", -1);
  test_data = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL, (GDestroyNotify) test_data_free);

  for (i = 0; lines[i]; i++)
    {
      TestData *data;
      gdouble traits[5];
      gchar *line;
      guint trait;

      line = lines[i];

      if (!line || !*line)
        continue;

      /* Read the trailing 5 doubles */
      for (trait = 0; trait < 5; trait++)
        {
          traits[trait] = g_ascii_strtod (line, &line);
          line += strlen (",");
        }

      data = test_data_new (traits[0], traits[1], traits[2], traits[3], traits[4], line);

      g_hash_table_insert (test_data, GUINT_TO_POINTER (i), data);
    }

  /* Close the file */
  if (!g_input_stream_close (G_INPUT_STREAM (input_stream), cancellable, error))
    return FALSE;

  if (out_test_data)
    *out_test_data = g_steal_pointer (&test_data);

  return TRUE;
}

static gint
compare_instances (GmflApplication *app,
                   GHashTable      *test_data,
                   GmflModelType    model_type,
                   GwString       **out_new_text,
                   guint            instance,
                   gboolean        *modified)
{
  g_autoptr (GmflPersonalityModifier) modifier = NULL;
  g_autoptr (GwDocument) document = NULL;
  g_autoptr (GwString) new_text = NULL;
  g_autoptr (GError) error = NULL;
  TestData *instance_data;

  instance_data = g_hash_table_lookup (test_data, GUINT_TO_POINTER (instance));

  /* Creates the document */
  document = gw_language_create_document_sync (gmfl_application_get_language (app),
                                               instance_data->text,
                                               NULL,
                                               &error);

  if (!document)
    {
      g_critical ("Error creating document: %s", error->message);
      return -1;
    }

  /* Setup the text modifier */
  modifier = gmfl_personality_modifier_new (gmfl_application_get_miner (app));
  gmfl_personality_modifier_set_personality (modifier, instance_data->personality);
  gmfl_personality_modifier_set_model_type (modifier, model_type);
  gmfl_personality_modifier_set_document (modifier, document);

  /* Rewrite the text */
  if (!gw_document_modify_sync (document, GW_MODIFIER (modifier), NULL, &error))
    {
      g_critical ("Error creating document: %s", error->message);
      return -1;
    }

  new_text = gw_document_get_text (document);

  g_print ("    Model: %d  |  Original: '%s'  |  New: '%s'\n",
           model_type,
           instance_data->text,
           new_text);

  if (modified)
    *modified = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (document), "modified"));

  if (out_new_text)
    *out_new_text = gw_string_ref (new_text);

  return gw_string_comparator_get_distance (GW_COMPARATOR_LEVENSHTEIN,
                                            instance_data->text,
                                            g_steal_pointer (&new_text));
}

static gboolean
run_tests (GmflApplication  *app,
           GFile            *file,
           GHashTable       *test_data,
           GCancellable     *cancellable,
           GError          **error)
{
  g_autoptr (GFileOutputStream) output_stream = NULL;
  g_autoptr (GString) string = NULL;
  g_autoptr (GFile) output_file = NULL;
  g_autofree gchar *path = NULL;
  guint sum_b5, sum_baseline;
  guint n_instances;
  guint i;

  sum_b5 = 0;
  sum_baseline = 0;
  n_instances = g_hash_table_size (test_data);
  output_file = g_file_new_for_path ("results.txt");
  output_stream = g_file_replace (output_file, NULL, FALSE, G_FILE_CREATE_NONE, cancellable, error);

  if (!output_stream)
    return FALSE;

  path = g_file_get_path (file);

  string = g_string_new ("");
  g_string_append_printf (string, "Testing %s\n", path);
  g_string_append (string, "==================================================================\n\n");
  g_string_append (string, "ID\tB5\tBASELINE\n");

  for (i = 0; i < n_instances; i++)
    {
      g_autoptr (GwString) baseline_text = NULL;
      g_autoptr (GwString) b5_text = NULL;
      gboolean b5_modified, baseline_modified;
      gint b5_distance, baseline_distance;

      b5_distance = compare_instances (app, test_data, GMFL_PERSONALITY_MODEL_B5, &b5_text, i, &b5_modified);
      baseline_distance = compare_instances (app, test_data, GMFL_PERSONALITY_MODEL_BASELINE, &baseline_text, i, &baseline_modified);

      /*
       * If both models didn't do anything, completely skip their
       * output. We can't measure an action that didn't happen.
       */
      if (!b5_modified && !baseline_modified)
        continue;

      g_string_append_printf (string, "%d\t%d\t%d", i, b5_distance, baseline_distance);

      /* Debug output if B5 lost the battle */
      if (b5_distance > baseline_distance)
        g_string_append_printf (string, "\t%s\t%s", b5_text, baseline_text);

      g_string_append (string, "\n");

      sum_b5 += b5_distance;
      sum_baseline += baseline_distance;
    }

  g_string_append_printf (string, "AVG\t%lf\t%lf\n", sum_b5 / (gdouble) n_instances, sum_baseline / (gdouble) n_instances);

  /* Write to the output file */
  if (!g_output_stream_write (G_OUTPUT_STREAM (output_stream), string->str, string->len, cancellable, error))
    return FALSE;

  /* And close it */
  if (!g_output_stream_close (G_OUTPUT_STREAM (output_stream), cancellable, error))
    return FALSE;

  return TRUE;
}

gboolean
gmfl_test_runner_run_test_for_file (GmflApplication  *app,
                                    GFile            *file,
                                    GCancellable     *cancellable,
                                    GError          **error)
{
  GHashTable *test_data = NULL;

  if (!load_test_data (file, &test_data, cancellable, error))
    return FALSE;

  if (!run_tests (app, file, test_data, cancellable, error))
    return FALSE;

  return TRUE;
}
