/* gmfl-personality-modifier.c
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "GmflPersonalityModifier"

#include "gmfl-application.h"
#include "gmfl-model.h"
#include "gmfl-model-miner.h"
#include "gmfl-personality.h"
#include "gmfl-personality-modifier.h"

#include <string.h>
#include <gwords.h>

struct _GmflPersonalityModifier
{
  GObject             parent;

  GwDocument         *document;

  GmflPersonality    *personality;
  GmflModelMiner     *miner;
  GmflModelType       model_type;
};

static void          gmfl_modifier_interface_init                (GwModifierInterface *iface);

G_DEFINE_TYPE_WITH_CODE (GmflPersonalityModifier, gmfl_personality_modifier, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GW_TYPE_MODIFIER, gmfl_modifier_interface_init))

enum
{
  PROP_0,
  PROP_MINER,
  PROP_PERSONALITY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS] = { NULL, };

/*
 * GwModifier iface
 */

static gboolean
gmfl_personality_modifier_modify_word (GwModifier      *modifier,
                                       const GwString  *word,
                                       gsize            len,
                                       GwString       **new_word,
                                       gsize           *new_len)
{
  GmflPersonalityModifier *self;
  GPtrArray *models;
  guint i;

  g_return_val_if_fail (GMFL_IS_PERSONALITY_MODIFIER (modifier), FALSE);

  self = GMFL_PERSONALITY_MODIFIER (modifier);
  models = gmfl_model_miner_get_models (self->miner, "pt_BR", self->model_type, "svm");

  for (i = 0; models && i < models->len; i++)
    {
      GmflModel *model;
      GwString *match;

      model = g_ptr_array_index (models, i);
      match = gmfl_model_match_word (model, self->personality, word);

      if (match)
        {
          *new_word = match;
          *new_len = g_utf8_strlen (match, -1);

          g_object_set_data (G_OBJECT (self->document), "modified", GUINT_TO_POINTER (TRUE));

          g_message ("Modified!");

          return TRUE;
        }
    }

  return FALSE;
}

static void
gmfl_modifier_interface_init (GwModifierInterface *iface)
{
  iface->modify_word = gmfl_personality_modifier_modify_word;
}

/*
 * GObject overrides
 */

static void
gmfl_personality_modifier_finalize (GObject *object)
{
  GmflPersonalityModifier *self = (GmflPersonalityModifier *)object;

  g_clear_object (&self->miner);
  g_clear_object (&self->personality);

  G_OBJECT_CLASS (gmfl_personality_modifier_parent_class)->finalize (object);
}

static void
gmfl_personality_modifier_get_property (GObject    *object,
                                        guint       prop_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
  GmflPersonalityModifier *self = GMFL_PERSONALITY_MODIFIER (object);

  switch (prop_id)
    {
    case PROP_MINER:
      g_value_set_object (value, self->personality);
      break;

    case PROP_PERSONALITY:
      g_value_set_object (value, self->personality);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_personality_modifier_set_property (GObject      *object,
                                        guint         prop_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
  GmflPersonalityModifier *self = GMFL_PERSONALITY_MODIFIER (object);

  switch (prop_id)
    {
    case PROP_MINER:
      self->miner = g_value_dup_object (value);
      g_assert_nonnull (self->miner);
      break;

    case PROP_PERSONALITY:
      gmfl_personality_modifier_set_personality (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_personality_modifier_class_init (GmflPersonalityModifierClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gmfl_personality_modifier_finalize;
  object_class->get_property = gmfl_personality_modifier_get_property;
  object_class->set_property = gmfl_personality_modifier_set_property;

  properties[PROP_PERSONALITY] = g_param_spec_object ("personality",
                                                      "Personality",
                                                      "Personality",
                                                      GMFL_TYPE_PERSONALITY,
                                                      G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  properties[PROP_MINER] = g_param_spec_object ("miner",
                                                "Miner",
                                                "Model miner",
                                                GMFL_TYPE_MODEL_MINER,
                                                G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gmfl_personality_modifier_init (GmflPersonalityModifier *self)
{
  self->model_type = GMFL_PERSONALITY_MODEL_B5;
}

GmflPersonalityModifier*
gmfl_personality_modifier_new (GmflModelMiner *miner)
{
  return g_object_new (GMFL_TYPE_PERSONALITY_MODIFIER,
                       "miner", miner,
                       NULL);
}

GmflModelType
gmfl_personality_modifier_get_model_type (GmflPersonalityModifier *self)
{
  g_return_val_if_fail (GMFL_IS_PERSONALITY_MODIFIER (self), -1);

  return self->model_type;
}

void
gmfl_personality_modifier_set_model_type (GmflPersonalityModifier *self,
                                          GmflModelType            model_type)
{
  g_return_if_fail (GMFL_IS_PERSONALITY_MODIFIER (self));

  if (self->model_type == model_type)
    return;

  self->model_type = model_type;
}

GmflPersonality*
gmfl_personality_modifier_get_personality (GmflPersonalityModifier *self)
{
  g_return_val_if_fail (GMFL_IS_PERSONALITY_MODIFIER (self), NULL);

  return self->personality;
}

void
gmfl_personality_modifier_set_personality (GmflPersonalityModifier *self,
                                           GmflPersonality         *personality)
{
  g_return_if_fail (GMFL_IS_PERSONALITY (personality));
  g_return_if_fail (GMFL_IS_PERSONALITY_MODIFIER (self));

  if (g_set_object (&self->personality, personality))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_PERSONALITY]);
}

void
gmfl_personality_modifier_set_document (GmflPersonalityModifier *self,
                                        GwDocument              *document)
{
  g_return_if_fail (GMFL_IS_PERSONALITY_MODIFIER (self));
  g_return_if_fail (GW_IS_DOCUMENT (document));

  g_set_object (&self->document, document);

  /* HACK!!! We should pass the GwDocument pointer to the vfunc */
  g_object_set_data (G_OBJECT (self->document), "modified", GUINT_TO_POINTER (FALSE));
}
