/* gmfl-model-miner.c
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "GmflModelMiner"

#include "gmfl-model.h"
#include "gmfl-model-libsvm.h"
#include "gmfl-model-miner.h"

#include <string.h>

struct _GmflModelMiner
{
  GObject             parent;

  GwLanguage         *language;

  GwRadixTree        *code_to_model;
};

G_DEFINE_TYPE (GmflModelMiner, gmfl_model_miner, G_TYPE_OBJECT)

enum
{
  PROP_0,
  PROP_LANGUAGE,
  N_PROPS
};

static GParamSpec *properties[N_PROPS] = { NULL, };

/*
 * Auxiliary methods
 */

static GPtrArray*
get_search_paths (void)
{
  GPtrArray *paths;
  const gchar * const *system_data_dirs;

  paths = g_ptr_array_new_with_free_func (g_free);

  /* System */
  system_data_dirs = g_get_system_data_dirs ();
  for (; *system_data_dirs; system_data_dirs++)
    g_ptr_array_add (paths, g_build_filename (*system_data_dirs, "gmafell", NULL));

  /* User */
  g_ptr_array_add (paths, g_build_filename (g_get_user_data_dir (), "gmafell", NULL));

  return paths;
}

static GmflModel*
create_model_for_type (GmflModelMiner *self,
                       GmflModelType   model_type,
                       const gchar    *algorithm,
                       const gchar    *path)
{
  struct {
    const gchar      *model_name;
    GType             gtype;
  } model_type_info[] = {
    { "svm", GMFL_TYPE_MODEL_LIBSVM },
  };

  guint i;

  for (i = 0; i < G_N_ELEMENTS (model_type_info); i++)
    {
      if (!g_str_has_prefix (algorithm, model_type_info[i].model_name))
        continue;

      return g_object_new (model_type_info[i].gtype,
                           "language", self->language,
                           "model-type", model_type,
                           "path", path,
                           NULL);
    }

  return NULL;
}

static const gchar*
get_algorithm_from_basename (GFile *file)
{
  g_autofree gchar *basename;

  basename = g_file_get_basename (file);

  if (strstr (basename, "svm"))
    return "svm";

  return NULL;
}

static GmflModelType
get_model_type_from_basename (GFile *file)
{
  g_autofree gchar *basename;

  basename = g_file_get_basename (file);

  if (strstr (basename, "b5"))
    return GMFL_PERSONALITY_MODEL_B5;
  else
    return GMFL_PERSONALITY_MODEL_BASELINE;
}

static const gchar*
model_type_name (GmflModelType type)
{
  switch (type)
    {
    case GMFL_PERSONALITY_MODEL_BASELINE:
      return "baseline";

    case GMFL_PERSONALITY_MODEL_B5:
      return "b5";
    }

  return NULL;
}

static void
load_models_from_directory (GmflModelMiner  *self,
                            GFile           *directory,
                            GCancellable    *cancellable,
                            GError         **error)
{
  g_autoptr (GFileEnumerator) enumerator = NULL;
  g_autoptr (GHashTable) model_type_paths = NULL;
  g_autofree gchar *dir_basename = NULL;
  g_autofree gchar *dir_path = NULL;
  GHashTableIter iter;
  GPtrArray *value;
  GError *local_error;
  gchar *key;

  local_error = NULL;
  dir_basename = g_file_get_basename (directory);
  dir_path = g_file_get_path (directory);
  enumerator = g_file_enumerate_children (directory,
                                          "standard:*",
                                          G_FILE_QUERY_INFO_NONE,
                                          cancellable,
                                          &local_error);

  if (local_error)
    {
      g_propagate_error (error, local_error);
      return;
    }

  model_type_paths = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

  while (TRUE)
    {
      GmflModelType model_type;
      const gchar *algorithm;
      GPtrArray *files;
      GFile *child;
      gchar *model;

      if (!g_file_enumerator_iterate (enumerator,
                                      NULL,
                                      &child,
                                      cancellable,
                                      &local_error))
        {
          g_propagate_error (error, local_error);
          return;
        }

      if (!child)
        break;

      /* Ignore non-regular files */
      if (g_file_query_file_type (child, G_FILE_QUERY_INFO_NONE, cancellable) != G_FILE_TYPE_REGULAR)
        continue;

      algorithm = get_algorithm_from_basename (child);

      /* Silently ignore non-model files */
      if (!algorithm)
        continue;

      model_type = get_model_type_from_basename (child);
      model = g_strdup_printf ("%s_%s", algorithm, model_type_name (model_type));

      /* Create a GPtrArray */
      if (!g_hash_table_contains (model_type_paths, model))
        {
          g_hash_table_insert (model_type_paths,
                               (gpointer) model,
                               g_ptr_array_new_with_free_func (g_object_unref));
        }

      files = g_hash_table_lookup (model_type_paths, model);
      g_ptr_array_add (files, g_object_ref (child));
    }

  /* Run through every entry and create a GmflModel implementation */
  g_hash_table_iter_init (&iter, model_type_paths);

  while (g_hash_table_iter_next (&iter, (gpointer*) &key, (gpointer*) &value))
    {
      g_autoptr (GmflModel) model = NULL;
      g_autofree gchar *code = NULL;
      GmflModelType model_type;
      GPtrArray *cached_models;
      gboolean result;

      /* TODO: ugh.... get rid of value->pdata[0] */
      model_type = get_model_type_from_basename (value->pdata[0]);
      model = create_model_for_type (self, model_type, key, dir_path);
      result = gmfl_model_load_sync (model, value, cancellable, &local_error);

      if (!result)
        {
          g_warning ("Error loading model: %s", local_error->message);
          g_propagate_error (error, local_error);
          break;
        }

      code = g_strdup_printf ("%s:%s", dir_basename, key);

      g_debug ("Found model of type %s for language %s, loaded with %s",
               key,
               dir_basename,
               g_type_name (G_OBJECT_TYPE (model)));

      if (!gw_radix_tree_contains (self->code_to_model, code, -1))
        {
          cached_models = g_ptr_array_new_with_free_func (g_object_unref);
          gw_radix_tree_insert (self->code_to_model, code, -1, cached_models);
        }
      else
        {
          cached_models = gw_radix_tree_lookup (self->code_to_model, code, -1, NULL);
        }

      g_ptr_array_add (cached_models, g_steal_pointer (&model));
    }
}

static void
try_load_directory (GmflModelMiner  *self,
                    const gchar     *path,
                    GCancellable    *cancellable,
                    GError         **error)
{
  g_autoptr (GFileEnumerator) enumerator = NULL;
  g_autoptr (GFile) file = NULL;
  GError *local_error = NULL;

  /* Don't do anything on non-existant folders */
  if (!g_file_test (path, G_FILE_TEST_EXISTS))
    return;

  file = g_file_new_for_path (path);
  enumerator = g_file_enumerate_children (file,
                                          "standard:*",
                                          G_FILE_QUERY_INFO_NONE,
                                          cancellable,
                                          &local_error);

  if (local_error)
    {
      g_propagate_error (error, local_error);
      return;
    }

  while (TRUE)
    {
      GFile *child;

      if (!g_file_enumerator_iterate (enumerator,
                                      NULL,
                                      &child,
                                      cancellable,
                                      &local_error))
        {
          g_propagate_error (error, local_error);
          return;
        }

      if (!child)
        return;

      /* Ignore regular files */
      if (g_file_query_file_type (child, G_FILE_QUERY_INFO_NONE, cancellable) == G_FILE_TYPE_REGULAR)
        continue;

      g_debug ("loading %s", path);

      load_models_from_directory (self, child, cancellable, error);
    }
}


/*
 * Async methods
 */

static void
miner_run_in_thread_cb (GTask        *task,
                        gpointer      source_object,
                        gpointer      task_data,
                        GCancellable *cancellable)
{
  GmflModelMiner *self;
  GError *error;
  gboolean result;

  self = GMFL_MODEL_MINER (source_object);
  error = NULL;
  result = gmfl_model_miner_run_sync (self, cancellable, &error);

  if (error)
    {
      g_task_return_error (task, error);
      return;
    }

  g_task_return_boolean (task, result);
}


/*
 * GObject overrides
 */

static void
gmfl_model_miner_finalize (GObject *object)
{
  GmflModelMiner *self = (GmflModelMiner *)object;

  g_clear_pointer (&self->code_to_model, g_hash_table_destroy);

  G_OBJECT_CLASS (gmfl_model_miner_parent_class)->finalize (object);
}

static void
gmfl_model_miner_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  GmflModelMiner *self = (GmflModelMiner *) object;

  switch (prop_id)
    {
    case PROP_LANGUAGE:
      g_value_set_object (value, self->language);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_model_miner_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  GmflModelMiner *self = (GmflModelMiner *) object;

  switch (prop_id)
    {
    case PROP_LANGUAGE:
      gmfl_model_miner_set_language (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_model_miner_class_init (GmflModelMinerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gmfl_model_miner_finalize;
  object_class->get_property = gmfl_model_miner_get_property;
  object_class->set_property = gmfl_model_miner_set_property;

  properties[PROP_LANGUAGE] = g_param_spec_object ("language",
                                                   "Language",
                                                   "Language",
                                                   GW_TYPE_LANGUAGE,
                                                   G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
gmfl_model_miner_init (GmflModelMiner *self)
{
  self->code_to_model = gw_radix_tree_new_with_free_func ((GDestroyNotify) g_ptr_array_unref);
}

GmflModelMiner*
gmfl_model_miner_new (void)
{
  return g_object_new (GMFL_TYPE_MODEL_MINER, NULL);
}

GwLanguage*
gmfl_model_miner_get_language (GmflModelMiner *self)
{
  g_return_val_if_fail (GMFL_IS_MODEL_MINER (self), NULL);

  return self->language;
}

void
gmfl_model_miner_set_language (GmflModelMiner *self,
                               GwLanguage     *language)
{
  g_return_if_fail (GMFL_IS_MODEL_MINER (self));
  g_return_if_fail (GW_IS_LANGUAGE (language));

  if (g_set_object (&self->language, language))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_LANGUAGE]);
}

void
gmfl_model_miner_run (GmflModelMiner      *self,
                      GCancellable        *cancellable,
                      GAsyncReadyCallback  callback,
                      gpointer             user_data)
{
  GTask *task;

  g_return_if_fail (GMFL_IS_MODEL_MINER (self));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_run_in_thread (task, miner_run_in_thread_cb);
}

gboolean
gmfl_model_miner_run_finish (GAsyncResult  *result,
                             GError       **error)
{
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

gboolean
gmfl_model_miner_run_sync (GmflModelMiner  *self,
                           GCancellable    *cancellable,
                           GError         **error)
{
  g_autoptr (GPtrArray) dirs = NULL;
  GError *local_error;
  guint i;

  g_return_val_if_fail (GMFL_IS_MODEL_MINER (self), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);

  dirs = get_search_paths ();
  local_error = NULL;

  for (i = 0; i < dirs->len; i++)
    try_load_directory (self, g_ptr_array_index (dirs, i), cancellable, &local_error);

  if (local_error)
    {
      g_propagate_error (error, local_error);
      return FALSE;
    }

  return TRUE;
}

GPtrArray*
gmfl_model_miner_get_models (GmflModelMiner *self,
                             const gchar    *language,
                             GmflModelType   type,
                             const gchar    *algorithm)
{
  g_autofree gchar *code = NULL;

  g_return_val_if_fail (GMFL_IS_MODEL_MINER (self), NULL);
  g_return_val_if_fail (language, NULL);
  g_return_val_if_fail (algorithm, NULL);

  code = g_strdup_printf ("%s:%s_%s", language, algorithm, model_type_name (type));

  return gw_radix_tree_lookup (self->code_to_model, code, -1, NULL);
}
