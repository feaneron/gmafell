/* gmfl-model-miner.h
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GMFL_MODEL_MINER_H
#define GMFL_MODEL_MINER_H

#include <gio/gio.h>
#include <glib-object.h>
#include <gwords.h>

G_BEGIN_DECLS

#define GMFL_TYPE_MODEL_MINER (gmfl_model_miner_get_type())

G_DECLARE_FINAL_TYPE (GmflModelMiner, gmfl_model_miner, GMFL, MODEL_MINER, GObject)

GmflModelMiner*      gmfl_model_miner_new                        (void);

GwLanguage*          gmfl_model_miner_get_language               (GmflModelMiner     *self);

void                 gmfl_model_miner_set_language               (GmflModelMiner     *self,
                                                                  GwLanguage         *language);

void                 gmfl_model_miner_run                        (GmflModelMiner     *self,
                                                                  GCancellable       *cancellable,
                                                                  GAsyncReadyCallback callback,
                                                                  gpointer            user_data);

gboolean             gmfl_model_miner_run_finish                 (GAsyncResult       *result,
                                                                  GError            **error);

gboolean             gmfl_model_miner_run_sync                   (GmflModelMiner     *self,
                                                                  GCancellable       *cancellable,
                                                                  GError            **error);

GPtrArray*           gmfl_model_miner_get_models                 (GmflModelMiner     *self,
                                                                  const gchar        *language,
                                                                  GmflModelType       type,
                                                                  const gchar        *algorithm);

G_END_DECLS

#endif /* GMFL_MODEL_MINER_H */

