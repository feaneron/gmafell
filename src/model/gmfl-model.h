/* gmfl-model.h
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GMFL_MODEL_H
#define GMFL_MODEL_H

#include "gmfl-types.h"

#include <gwords.h>
#include <gio/gio.h>
#include <glib-object.h>

G_BEGIN_DECLS

typedef enum
{
  GMFL_MODEL_QUIRK_INVALID            = -1,
  GMFL_MODEL_QUIRK_NONE               =  0,
  GMFL_MODEL_QUIRK_SUPPORT_ADJECTIVES =  1 << 0,
  GMFL_MODEL_QUIRK_SUPPORT_NOUNS      =  1 << 1,
  GMFL_MODEL_QUIRK_SUPPORT_VERBS      =  1 << 2,
} GmflModelQuirks;

typedef enum
{
  GMFL_MODEL_ERROR_INVALID,
  GMFL_MODEL_ERROR_INCOMPLETE,
} GmflModelError;

#define GMFL_MODEL_ERROR (gmfl_model_error_quark ())
#define GMFL_TYPE_MODEL (gmfl_model_get_type ())

G_DECLARE_INTERFACE (GmflModel, gmfl_model, GMFL, MODEL, GObject)

struct _GmflModelInterface
{
  GTypeInterface parent;

  gboolean           (*load)                                     (GmflModel          *self,
                                                                  GPtrArray          *files,
                                                                  GCancellable       *cancellable,
                                                                  GError            **error);

  GwString*          (*match_word)                               (GmflModel          *self,
                                                                  GmflPersonality    *personality,
                                                                  const GwString     *word);
};

GQuark               gmfl_model_error_quark                      (void);

GwLanguage*          gmfl_model_get_language                     (GmflModel          *self);

GmflModelType        gmfl_model_get_model_type                   (GmflModel          *self);

void                 gmfl_model_load                             (GmflModel          *self,
                                                                  GPtrArray          *files,
                                                                  GCancellable       *cancellable,
                                                                  GAsyncReadyCallback callback,
                                                                  gpointer            user_data);

gboolean             gmfl_model_load_finish                      (GAsyncResult       *result,
                                                                  GError            **error);

gboolean             gmfl_model_load_sync                        (GmflModel          *self,
                                                                  GPtrArray          *files,
                                                                  GCancellable       *cancellable,
                                                                  GError            **error);

GwString*            gmfl_model_match_word                       (GmflModel          *self,
                                                                  GmflPersonality    *personality,
                                                                  const GwString     *word);

G_END_DECLS

#endif /* GMFL_MODEL_H */
