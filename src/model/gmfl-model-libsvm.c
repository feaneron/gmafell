/* gmfl-model-libsvm.c
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "GmflModelLibsvm"

#include "gmfl-model.h"
#include "gmfl-model-libsvm.h"
#include "gmfl-personality.h"

#include "svm.h"

#include <gwords.h>

struct _GmflModelLibsvm
{
  GObject             parent;

  GwLanguage         *language;
  GmflPersonality    *personality;

  struct svm_model   *models[3];

  GwRadixTree        *word_to_concept;
  GwRadixTree        *concept_to_number;
  GwRadixTree        *number_to_word;

  gchar              *path;

  GmflModelType       model_type;
};

static void          gmfl_model_iface_init                       (GmflModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (GmflModelLibsvm, gmfl_model_libsvm, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GMFL_TYPE_MODEL, gmfl_model_iface_init));

enum
{
  PROP_0,
  PROP_LANGUAGE,
  PROP_MODEL_TYPE,
  PROP_PATH,
  N_PROPS
};


enum
{
  INVALID   = -1,
  ADJECTIVE =  0,
  NOUN      =  1,
  VERB      =  2,
  N_TYPES
};

/*
 * Auxiliary methods
 */

static const gchar*
get_concept_from_word (GmflModelLibsvm *self,
                       const GwString  *word)
{
  GPtrArray *concepts;

  concepts = gw_radix_tree_lookup (self->word_to_concept, word, -1, NULL);

  /* TODO: use a better heuristic to determine the best concept. Perhaps the most frequent one? */
  if (concepts)
    return g_ptr_array_index (concepts, 0);

  return NULL;
}

static const gchar*
get_word_from_class_id (GmflModelLibsvm *self,
                        gint             id,
                        gint             wclass)
{
  gchar number[30] = { 0, };

  g_snprintf (number, 30, "%d_%d", wclass, id);

  return gw_radix_tree_lookup (self->number_to_word, number, -1, NULL);
}

static gboolean
validate_predicted_result (GmflModelLibsvm *self,
                           const GwString  *word,
                           const GwString  *predicted)
{
  GPtrArray *word_concepts, *predicted_concepts;
  guint i, j;

  word_concepts = gw_radix_tree_lookup (self->word_to_concept, word, -1, NULL);
  predicted_concepts = gw_radix_tree_lookup (self->word_to_concept, predicted, -1, NULL);

  if (!word_concepts || !predicted_concepts)
    return FALSE;

  for (i = 0; i < word_concepts->len; i++)
    {
      const gchar *w_concept;

      w_concept = g_ptr_array_index (word_concepts, i);

      for (j = 0; j < predicted_concepts->len; j++)
        {
          const gchar *p_concept;

          p_concept = g_ptr_array_index (predicted_concepts, j);

          if (g_strcmp0 (w_concept, p_concept) == 0)
            return TRUE;
        }
    }

  return FALSE;
}

#if 0
static void
print_nodes (struct svm_node *nodes)
{
  g_print ("\n");

  while (nodes && (*nodes).index)
    {
      g_print ("%d:%lf ", nodes->index, nodes->value);
      nodes++;
    }

  g_print ("\n");
}
#endif

static GwGrammarClass
get_grammar_class_from_type (gint type)
{
  switch (type)
    {
    case ADJECTIVE:
      return GW_GRAMMAR_CLASS_ADJECTIVE;

    case NOUN:
      return GW_GRAMMAR_CLASS_NOUN;

    case VERB:
      return GW_GRAMMAR_CLASS_VERB;
    }

  return -1;
}

static struct svm_node*
build_svm_nodes (GmflModelLibsvm *self,
                 GwGrammarClass   grammar_class,
                 GmflPersonality *personality,
                 const GwString  *word)
{
  struct
  {
    gdouble trait;
    gint    index;
  } traits[5];

  g_autofree struct svm_node *nodes = NULL;
  g_autoptr (GwWord) w = NULL;
  const gchar *concept;
  GwDictionary *dict;
  const GwString *real_word;
  gdouble agreeableness;
  gdouble consciousness;
  gdouble extroversion;
  gdouble culturality;
  gdouble stability;
  guint concept_id;
  gint n_fields;
  gint n_traits;
  gint index;
  gint i;

  gmfl_personality_get_traits (personality,
                               &extroversion,
                               &stability,
                               &agreeableness,
                               &consciousness,
                               &culturality);

  dict = gw_language_get_dictionary (self->language);
  w = gw_dictionary_lookup (dict, grammar_class, (GwString*) word, -1);

  if (w)
    real_word = gw_word_get_canonical (w);
  else
    real_word = word;

  index = 0;
  n_traits = 0;

#define count_add_trait(x) G_STMT_START { \
  index++;                                \
  if (x > 0)                              \
    {                                     \
      traits[n_traits].trait = x;         \
      traits[n_traits++].index = index;   \
    }                                     \
  } G_STMT_END;

  if (self->model_type == GMFL_PERSONALITY_MODEL_B5)
    {
      count_add_trait (extroversion);
      count_add_trait (agreeableness);
      count_add_trait (consciousness);
      count_add_trait (stability);
      count_add_trait (culturality);
    }

#undef count_add_trait

  /* Effectively create the nodes */
  n_fields = n_traits + 2;
  nodes = g_new0 (struct svm_node, n_fields);

  for (i = 0; i < n_traits; i++)
    {
      struct svm_node *node;

      node = &nodes[i];

      /* Normal node */
      node->index = traits[i].index;
      node->value = traits[i].trait;
    }

  /* Retrieve the concept. If NULL, this word is not in our dictionary */
  concept = get_concept_from_word (self, real_word);

  if (!concept)
    return NULL;

  /* Add the 'Concept' node */
  concept_id = GPOINTER_TO_UINT (gw_radix_tree_lookup (self->concept_to_number, concept, -1, NULL));

  nodes[n_traits].index = concept_id;
  nodes[n_traits].value = 1;

  if (self->model_type == GMFL_PERSONALITY_MODEL_B5)
    nodes[n_traits].index += 5;

  /* Last node */
  nodes[n_fields - 1].index = -1;
  nodes[n_fields - 1].value = 0;

  return g_steal_pointer (&nodes);
}

static gboolean
load_word_to_concept_map (GmflModelLibsvm  *self,
                          GFile            *word_to_concept,
                          GCancellable     *cancellable,
                          GError          **error)
{
  g_autofree gchar *contents = NULL;
  g_autofree gchar *path = NULL;
  GStrv lines;
  guint i, j;

  if (!g_file_query_exists (word_to_concept, cancellable) ||
      g_file_query_file_type (word_to_concept, G_FILE_QUERY_INFO_NONE, cancellable) != G_FILE_TYPE_REGULAR)
    {
      g_set_error (error,
                   GMFL_MODEL_ERROR,
                   GMFL_MODEL_ERROR_INCOMPLETE,
                   "The model is missing the word-to-concept mapping");
      return FALSE;
    }

  path = g_file_get_path (word_to_concept);

  if (!g_file_get_contents (path, &contents, NULL, error))
    return FALSE;

  lines = g_strsplit (contents, "\n", -1);

  for (i = 0; lines[i] && *lines[i]; i++)
    {
      const gchar *concept;
      const gchar *line;
      GStrv split_line;
      GStrv words;

      line = lines[i];
      split_line = g_strsplit (line, "\t", 3);

      concept = split_line[0];
      words = g_strsplit (split_line[2], ",", -1);

      for (j = 0; words[j] && *words[j]; j++)
        {
          GPtrArray *concepts;
          const gchar *word;

          word = words[j];

          if (!gw_radix_tree_contains (self->word_to_concept, word, -1))
            {
              concepts = g_ptr_array_new_with_free_func (g_free);
              gw_radix_tree_insert (self->word_to_concept, word, -1, concepts);
            }
          else
            {
              concepts = gw_radix_tree_lookup (self->word_to_concept, word, -1, NULL);
            }

          g_ptr_array_add (concepts, g_strdup (concept));
        }

      g_clear_pointer (&split_line, g_strfreev);
      g_clear_pointer (&words, g_strfreev);
    }

  g_clear_pointer (&lines, g_strfreev);

  return TRUE;
}

static gboolean
load_concept_to_number_map (GmflModelLibsvm  *self,
                            GFile            *concept_to_number,
                            GCancellable     *cancellable,
                            GError          **error)
{
  g_autofree gchar *contents = NULL;
  g_autofree gchar *path = NULL;
  GStrv lines;
  guint counters[3] = { 1, 1, 1 };
  guint i;
  gint current;

  if (!g_file_query_exists (concept_to_number, cancellable) ||
      g_file_query_file_type (concept_to_number, G_FILE_QUERY_INFO_NONE, cancellable) != G_FILE_TYPE_REGULAR)
    {
      g_set_error (error,
                   GMFL_MODEL_ERROR,
                   GMFL_MODEL_ERROR_INCOMPLETE,
                   "The model is missing the concept-to-number mapping");
      return FALSE;
    }

  path = g_file_get_path (concept_to_number);

  if (!g_file_get_contents (path, &contents, NULL, error))
    return FALSE;

  current = INVALID;
  lines = g_strsplit (contents, "\n", -1);

  for (i = 0; lines[i] && *lines[i]; i++)
    {
      /* Sentinel line */
      if (g_str_equal (lines[i], "ADJECTIVES"))
        {
          current = ADJECTIVE;
          continue;
        }
      else if (g_str_equal (lines[i], "NOUNS"))
        {
          current = NOUN;
          continue;
        }
      else if (g_str_equal (lines[i], "VERBS"))
        {
          current = VERB;
          continue;
        }
      else
        g_assert (current != INVALID);

      /* Add to the associative tree */
      gw_radix_tree_insert (self->concept_to_number,
                            lines[i],
                            -1,
                            GUINT_TO_POINTER (counters[current]++));
    }

  g_clear_pointer (&lines, g_strfreev);

  return TRUE;
}

static gboolean
load_word_order_map (GmflModelLibsvm  *self,
                     GFile            *word_order,
                     GCancellable     *cancellable,
                     GError          **error)
{
  g_autofree gchar *contents = NULL;
  g_autofree gchar *path = NULL;
  GStrv lines;
  guint counters[3] = { 0, };
  guint i;
  gint current;

  if (!g_file_query_exists (word_order, cancellable) ||
      g_file_query_file_type (word_order, G_FILE_QUERY_INFO_NONE, cancellable) != G_FILE_TYPE_REGULAR)
    {
      g_set_error (error,
                   GMFL_MODEL_ERROR,
                   GMFL_MODEL_ERROR_INCOMPLETE,
                   "The model is missing the word order mapping");
      return FALSE;
    }

  path = g_file_get_path (word_order);

  if (!g_file_get_contents (path, &contents, NULL, error))
    return FALSE;

  current = INVALID;
  lines = g_strsplit (contents, "\n", -1);

  for (i = 0; lines[i] && *lines[i]; i++)
    {
      gchar number_str[30] = { 0, };

      /* Sentinel line */
      if (g_str_equal (lines[i], "ADJECTIVES"))
        {
          current = ADJECTIVE;
          continue;
        }
      else if (g_str_equal (lines[i], "NOUNS"))
        {
          current = NOUN;
          continue;
        }
      else if (g_str_equal (lines[i], "VERBS"))
        {
          current = VERB;
          continue;
        }
      else
        g_assert (current != INVALID);

      g_snprintf (number_str, 30, "%d_%d", current, counters[current]++);

      /* Add to the associative tree */
      gw_radix_tree_insert (self->number_to_word, number_str, -1, g_strdup (lines[i]));
    }

  g_clear_pointer (&lines, g_strfreev);

  return TRUE;
}

static gboolean
load_map_files (GmflModelLibsvm  *self,
                GCancellable     *cancellable,
                GError          **error)
{
  g_autoptr (GFile) concept_to_number = NULL;
  g_autoptr (GFile) word_to_concept = NULL;
  g_autoptr (GFile) word_order = NULL;
  g_autoptr (GFile) base_path = NULL;

  base_path = g_file_new_for_path (self->path);

  /*
   * Word to concept
   */
  word_to_concept = g_file_get_child (base_path, "word_to_concept");

  if (!load_word_to_concept_map (self, word_to_concept, cancellable, error))
    return FALSE;

  /*
   * Concept to number
   */
  concept_to_number = g_file_get_child (base_path, "concept_to_number");

  if (!load_concept_to_number_map (self, concept_to_number, cancellable, error))
    return FALSE;

  /*
   * Number to word
   */
  word_order = g_file_get_child (base_path, "word_order");

  if (!load_word_order_map (self, word_order, cancellable, error))
    return FALSE;

  return TRUE;
}

/*
 * GmflModel interface
 */

static gboolean
gmfl_model_libsvm_load (GmflModel     *model,
                        GPtrArray     *files,
                        GCancellable  *cancellable,
                        GError       **error)
{
  GmflModelLibsvm *self;
  guint i;

  g_return_val_if_fail (GMFL_IS_MODEL_LIBSVM (model), FALSE);

  self = GMFL_MODEL_LIBSVM (model);

  /* Load the mapping files */
  if (!load_map_files (self, cancellable, error))
    return FALSE;

  /* Then the SVM models */
  for (i = 0; i < files->len; i++)
    {
      struct svm_model *model;
      g_autofree gchar *name;
      g_autofree gchar *path;
      GFile *file;

      file = g_ptr_array_index (files, i);
      name = g_file_get_basename (file);
      path = g_file_get_path (file);

      /*
       * Check if operation was cancelled ~before~ loading
       * the model.
       */
      if (g_cancellable_set_error_if_cancelled (cancellable, error))
        return FALSE;

      /* Load the main model */
      model = svm_load_model (path);

      if (!model)
        {
          g_set_error (error, GMFL_MODEL_ERROR, GMFL_MODEL_ERROR_INVALID, "Invalid LibSVM model");
          return FALSE;
        }

      /* Set the correct struct field */
      if (g_str_has_prefix (name, "adjectives"))
        self->models[ADJECTIVE] = model;
      else if (g_str_has_prefix (name, "nouns"))
        self->models[NOUN] = model;
      else if (g_str_has_prefix (name, "verbs"))
        self->models[VERB] = model;
      else
        g_assert_not_reached ();
    }

  return TRUE;
}

static GwString*
gmfl_model_libsvm_match_word (GmflModel       *model,
                              GmflPersonality *personality,
                              const GwString  *word)
{
  GmflModelLibsvm *self;
  gint type;

  g_return_val_if_fail (GMFL_IS_MODEL_LIBSVM (model), NULL);
  g_return_val_if_fail (GMFL_IS_PERSONALITY (personality), NULL);
  g_return_val_if_fail (word, NULL);

  self = GMFL_MODEL_LIBSVM (model);

#if 0
  for (type = ADJECTIVE; type < N_TYPES; type++)
    {
      GwGrammarClass grammar_class;
      struct svm_node *svm_input;
      const gchar *predicted_word;
      gdouble prediction;

      grammar_class = get_grammar_class_from_type (type);
      svm_input = build_svm_nodes (self, grammar_class, personality, word);

      if (!svm_input)
        return NULL;

      prediction = svm_predict (self->models[type], svm_input);
      predicted_word = get_word_from_class_id (self, (gint) prediction, type);

      if (validate_predicted_result (self, word, predicted_word))
        {
          g_debug ("Replacing '%s' by '%s'", word, predicted_word);
          return gw_string_new (predicted_word);
        }
    }
#endif

  GwGrammarClass grammar_class;
  struct svm_node *svm_input;
  const gchar *predicted_word;
  gdouble prediction;

  type = ADJECTIVE;

  grammar_class = get_grammar_class_from_type (type);
  svm_input = build_svm_nodes (self, grammar_class, personality, word);

  if (!svm_input)
    return NULL;

  prediction = svm_predict (self->models[type], svm_input);
  predicted_word = get_word_from_class_id (self, (gint) prediction, type);

  if (validate_predicted_result (self, word, predicted_word))
    {
      g_debug ("Replacing '%s' by '%s'", word, predicted_word);
      return gw_string_new (predicted_word);
    }

  return NULL;
}

static void
gmfl_model_iface_init (GmflModelInterface *iface)
{
  iface->load = gmfl_model_libsvm_load;
  iface->match_word = gmfl_model_libsvm_match_word;
}


/*
 * GObject overrides
 */

static void
gmfl_model_libsvm_finalize (GObject *object)
{
  GmflModelLibsvm *self = (GmflModelLibsvm *)object;

  svm_free_and_destroy_model (&self->models[ADJECTIVE]);
  svm_free_and_destroy_model (&self->models[NOUN]);
  svm_free_and_destroy_model (&self->models[VERB]);

  g_clear_pointer (&self->path, g_free);
  g_clear_object (&self->language);

  G_OBJECT_CLASS (gmfl_model_libsvm_parent_class)->finalize (object);
}

static void
gmfl_model_libsvm_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  GmflModelLibsvm *self = GMFL_MODEL_LIBSVM (object);

  switch (prop_id)
    {
    case PROP_LANGUAGE:
      g_value_set_object (value, self->language);
      break;

    case PROP_MODEL_TYPE:
      g_value_set_int (value, self->model_type);
      break;

    case PROP_PATH:
      g_value_set_string (value, self->path);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_model_libsvm_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  GmflModelLibsvm *self = GMFL_MODEL_LIBSVM (object);

  switch (prop_id)
    {
    case PROP_LANGUAGE:
      self->language = g_value_dup_object (value);
      break;

    case PROP_MODEL_TYPE:
      self->model_type = g_value_get_int (value);
      break;

    case PROP_PATH:
      self->path = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
gmfl_model_libsvm_class_init (GmflModelLibsvmClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = gmfl_model_libsvm_finalize;
  object_class->get_property = gmfl_model_libsvm_get_property;
  object_class->set_property = gmfl_model_libsvm_set_property;

  g_object_class_override_property (object_class, PROP_LANGUAGE, "language");
  g_object_class_override_property (object_class, PROP_MODEL_TYPE, "model-type");
  g_object_class_override_property (object_class, PROP_PATH, "path");
}

static void
gmfl_model_libsvm_init (GmflModelLibsvm *self)
{
  self->word_to_concept = gw_radix_tree_new_with_free_func (g_free);
  self->concept_to_number = gw_radix_tree_new_with_free_func (g_free);
  self->number_to_word = gw_radix_tree_new_with_free_func (g_free);
}

GmflModelLibsvm*
gmfl_model_libsvm_new (GwLanguage    *language,
                       GmflModelType  model_type)
{
  return g_object_new (GMFL_TYPE_MODEL_LIBSVM,
                       "model-type", model_type,
                       "language", language,
                       NULL);
}
