/* gmfl-model.c
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gmfl-model.h"
#include "gmfl-personality.h"

#include <gio/gio.h>

G_DEFINE_INTERFACE (GmflModel, gmfl_model, G_TYPE_OBJECT)

static void
load_in_thread_cb (GTask        *task,
                   gpointer      source_object,
                   gpointer      task_data,
                   GCancellable *cancellable)
{
  GmflModel *self;
  GError *error;
  gboolean result;

  self = GMFL_MODEL (source_object);
  error = NULL;
  result = gmfl_model_load_sync (self, task_data, cancellable, &error);

  if (error)
    {
      g_task_return_error (task, error);
      return;
    }

  g_task_return_boolean (task, result);
}

static void
gmfl_model_default_init (GmflModelInterface *iface)
{
  g_object_interface_install_property (iface,
                                       g_param_spec_object ("language",
                                                            "Language",
                                                            "Language of the model",
                                                            GW_TYPE_LANGUAGE,
                                                            G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
  g_object_interface_install_property (iface,
                                       g_param_spec_int ("model-type",
                                                         "Model type",
                                                         "The type of the model",
                                                         0, 1, 0,
                                                         G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  g_object_interface_install_property (iface,
                                       g_param_spec_string ("path",
                                                            "Filesystem path",
                                                            "The path to the model",
                                                            NULL,
                                                            G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));
}

GQuark
gmfl_model_error_quark (void)
{
  return g_quark_from_static_string ("Error loading model");
}

GwLanguage*
gmfl_model_get_language (GmflModel *self)
{
  g_autoptr (GwLanguage) language = NULL;

  g_return_val_if_fail (GMFL_IS_MODEL (self), NULL);

  g_object_get (self, "language", &language, NULL);

  return language;
}

GmflModelType
gmfl_model_get_model_type (GmflModel *self)
{
  GmflModelType model_type;

  g_return_val_if_fail (GMFL_IS_MODEL (self), -1);

  g_object_get (self, "model-type", &model_type, NULL);

  return model_type;
}

void
gmfl_model_load (GmflModel           *self,
                 GPtrArray           *files,
                 GCancellable        *cancellable,
                 GAsyncReadyCallback  callback,
                 gpointer             user_data)
{
  GTask *task;

  g_return_if_fail (GMFL_IS_MODEL (self));

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_task_data (task, g_ptr_array_ref (files), (GDestroyNotify) g_ptr_array_unref);
  g_task_run_in_thread (task, load_in_thread_cb);
}

gboolean
gmfl_model_load_finish (GAsyncResult  *result,
                        GError       **error)
{
  g_return_val_if_fail (G_IS_TASK (result), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

gboolean
gmfl_model_load_sync (GmflModel     *self,
                      GPtrArray     *files,
                      GCancellable  *cancellable,
                      GError       **error)
{
  g_return_val_if_fail (GMFL_IS_MODEL (self), FALSE);
  g_return_val_if_fail (!error || !*error, FALSE);
  g_return_val_if_fail (GMFL_MODEL_GET_IFACE (self)->load, FALSE);

  return GMFL_MODEL_GET_IFACE (self)->load (self, files, cancellable, error);
}

GwString*
gmfl_model_match_word (GmflModel       *self,
                       GmflPersonality *personality,
                       const GwString  *word)
{
  g_return_val_if_fail (GMFL_IS_MODEL (self), NULL);
  g_return_val_if_fail (GMFL_IS_PERSONALITY (personality), NULL);
  g_return_val_if_fail (word, NULL);
  g_return_val_if_fail (GMFL_MODEL_GET_IFACE (self)->match_word, NULL);

  return GMFL_MODEL_GET_IFACE (self)->match_word (self, personality, word);
}
