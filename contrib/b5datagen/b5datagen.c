/* b5datagen.c
 *
 * Copyright (C) 2017 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define G_LOG_DOMAIN "B5DataGen"

#include <glib.h>
#include <gio/gio.h>
#include <locale.h>

/*
 * Global vars
 */

static GHashTable *captions = NULL;
static gchar      *captions_path = NULL;
static gchar      *output_path = "output.txt";
static guint       picture = 0;
static GHashTable *subjects = NULL;
static gchar      *subjects_path = NULL;

static GOptionEntry entries[] =
{
  { "subject", 's', 0, G_OPTION_ARG_FILENAME, &subjects_path, "File containing subjects information",  "SUBJECTS" },
  { "caption", 'c', 0, G_OPTION_ARG_FILENAME, &captions_path, "File containing conptions information", "CAPTIONS" },
  { "picture", 'p', 0, G_OPTION_ARG_INT,      &picture,       "Picture number",                        "PICTURE"  },
  { "output",  'o', 0, G_OPTION_ARG_FILENAME, &output_path,   "Output file path",                      "OUTPUT"   },
  { NULL }
};

typedef struct
{
  guint64             id;
  gdouble             traits[5];
} SubjectData;

typedef struct
{
  guint               picture;
  guint64             subject_id;
  gchar              *text;
} CaptionData;

/**********************************************************************************************************************/

static gboolean
load_subjects (GFile   *file,
               GError **error)
{
  g_autoptr (GFileInputStream) input_stream = NULL;
  g_autoptr (GFileInfo) file_info = NULL;
  g_autofree gchar *contents = NULL;
  g_autofree GStrv lines = NULL;
  guint64 file_size;
  guint i;

  /* Initialize 'subjects' hashtable */
  subjects = g_hash_table_new (g_direct_hash, g_direct_equal);

  /* Open input stream */
  input_stream = g_file_read (file, NULL, error);

  if (!input_stream)
    return FALSE;

  /* Retrieve file size */
  file_info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_SIZE, G_FILE_QUERY_INFO_NONE, NULL, error);

  if (!file_info)
    return FALSE;

  file_size = g_file_info_get_attribute_uint64 (file_info, G_FILE_ATTRIBUTE_STANDARD_SIZE);

  /* Read file contents into memory */
  contents = g_malloc0 (file_size);

  if (!g_input_stream_read (G_INPUT_STREAM (input_stream), contents, file_size, NULL, error))
    return FALSE;

  /* Parse each line */
  lines = g_strsplit (contents, "\n", -1);

  for (i = 0; lines[i]; i++)
    {
      g_autofree GStrv fields = NULL;
      SubjectData *data;
      gchar *line;
      guint trait;

      line = lines[i];

      if (!line || !*line)
        continue;

      fields = g_strsplit (line, ",", -1);

      /* Create the SubjectData from the line */
      data = g_slice_new (SubjectData);
      data->id = g_ascii_strtoull (fields[0], NULL, 0);

      for (trait = 0; trait < 5; trait++)
        data->traits[trait] = g_ascii_strtod (fields[8 + trait], NULL);

      g_debug ("Creating subject %ld (%lf, %lf, %lf, %lf, %lf)",
               data->id,
               data->traits[0],
               data->traits[1],
               data->traits[2],
               data->traits[3],
               data->traits[4]);

      g_hash_table_insert (subjects, GUINT_TO_POINTER (data->id), data);
    }

  /* Close the input stream */
  if (!g_input_stream_close (G_INPUT_STREAM (input_stream), NULL, error))
    return FALSE;

  return TRUE;
}

static gboolean
load_captions (GFile   *file,
               GError **error)
{
  g_autoptr (GFileInputStream) input_stream = NULL;
  g_autoptr (GFileInfo) file_info = NULL;
  g_autofree gchar *contents = NULL;
  g_autofree GStrv lines = NULL;
  guint64 file_size;
  guint i;

  /* Initialize 'captions' hashtable */
  captions = g_hash_table_new (g_direct_hash, g_direct_equal);

  /* Open input stream */
  input_stream = g_file_read (file, NULL, error);

  if (!input_stream)
    return FALSE;

  /* Retrieve file size */
  file_info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_SIZE, G_FILE_QUERY_INFO_NONE, NULL, error);

  if (!file_info)
    return FALSE;

  file_size = g_file_info_get_attribute_uint64 (file_info, G_FILE_ATTRIBUTE_STANDARD_SIZE);

  /* Read file contents into memory */
  contents = g_malloc0 (file_size);

  if (!g_input_stream_read (G_INPUT_STREAM (input_stream), contents, file_size, NULL, error))
    return FALSE;

  /* Parse each line */
  lines = g_strsplit (contents, "\n", -1);

  for (i = 0; lines[i]; i++)
    {
      g_autofree GStrv fields = NULL;
      CaptionData *data;
      gchar *line;

      line = lines[i];

      if (!line || !*line)
        continue;

      fields = g_strsplit (line, ", ", 2);

      /* Create the SubjectData from the line */
      data = g_slice_new (CaptionData);
      data->picture = picture;
      data->subject_id = g_ascii_strtoull (fields[0], NULL, 0);
      data->text = g_strdup (fields[1]);

      g_debug ("Creating caption %d_%ld: %s",
               data->picture,
               data->subject_id,
               data->text);

      g_hash_table_insert (captions, GUINT_TO_POINTER (i), data);
    }

  /* Close the input stream */
  if (!g_input_stream_close (G_INPUT_STREAM (input_stream), NULL, error))
    return FALSE;

  return TRUE;
}

static gboolean
serialize_data (GFile   *file,
                GError **error)
{
  g_autoptr (GFileOutputStream) output_stream = NULL;
  GHashTableIter iter;
  CaptionData *value;
  guint64 key;

  setlocale (LC_NUMERIC, "C");

  /* Open the output file */
  output_stream = g_file_replace (file, NULL, FALSE, G_FILE_CREATE_NONE, NULL, error);

  if (!output_stream)
    return FALSE;

  /* Gather the data and save to the output file */
  g_hash_table_iter_init (&iter, captions);

  while (g_hash_table_iter_next (&iter, (gpointer*) &key, (gpointer*) &value))
    {
      SubjectData *subject;
      GString *line;
      guint i;

      subject = g_hash_table_lookup (subjects, GUINT_TO_POINTER (value->subject_id));
      line = g_string_new ("");

      for (i = 0; i < 5; i++)
        g_string_append_printf (line, "%f,", subject->traits[i]);

      g_string_append (line, value->text);
      g_string_append (line, "\n");

      /* Save to the output file */
      if (!g_output_stream_write (G_OUTPUT_STREAM (output_stream), line->str, line->len, NULL, error))
        return FALSE;

      g_string_free (line, TRUE);
    }

  /* Close the input stream */
  if (!g_output_stream_close (G_OUTPUT_STREAM (output_stream), NULL, error))
    return FALSE;

  return TRUE;
}

static gboolean
generate_data (GError **error)
{
  g_autoptr (GFile) captions_file;
  g_autoptr (GFile) subjects_file;
  g_autoptr (GFile) output_file;

  captions_file = g_file_new_for_path (captions_path);
  subjects_file = g_file_new_for_path (subjects_path);
  output_file = g_file_new_for_path (output_path);

  /* Subjects */
  if (!load_subjects (subjects_file, error))
    return FALSE;

  /* Captions */
  if (!load_captions (captions_file, error))
    return FALSE;

  /* Save to output */
  if (!serialize_data (output_file, error))
    return FALSE;

  return TRUE;
}


/**********************************************************************************************************************/

#include <glib.h>

gint
main (gint   argc,
      gchar *argv[])
{
  GOptionContext *context;
  GError *error;

  error = NULL;
  context = g_option_context_new ("- Big5 subject data generator");
  g_option_context_add_main_entries (context, entries, NULL);
  g_option_context_set_summary (context, "Generates a list of texts with the personality of the writer, in order to "
                                         "provide batch test data for GMafell");

  g_set_prgname ("b5datagen");
  g_set_application_name ("B5datagen");

  /* Parse arguments */
  if (!g_option_context_parse (context, &argc, &argv, &error) ||
      !captions_path ||
      !subjects_path ||
      picture == 0)
    {
      if (error)
        g_printerr ("Error parsing input arguments: %s\n", error->message);

      g_printerr ("%s\n", g_option_context_get_help (context, TRUE, NULL));
      return 1;
    }

  /* Generate the data files */
  if (!generate_data (&error))
    {
      g_printerr ("Error generating data: %s\n", error->message);
      return 1;
    }

  return 0;
}
